/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require('fs');
const path = require('path');

// Store data to be put into Json File.
let ContentOfJsonFile = require('./data.json');

// Function to create directory to store json files in it.
function createJSONDirectory(directoryName) {
  return new Promise((resolve, reject) => {
    if (directoryName === undefined) {
      reject('Directory name is undefined');
    } else if (typeof directoryName !== 'string') {
      reject('Directory name is not of type string');
    } else {
      const directoryPath = path.join(__dirname, directoryName);
      fs.mkdir(directoryPath, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(directoryPath);
        }
      });
    }
  });
}

// Function to create files in directory created.
function createJSONFiles(directoryPath, fileNames) {
  return new Promise((resolve, reject) => {
    if (fileNames.length == 0) {
      reject('There are no files to be created');
    } else {
      // to store errors while creating file in directory
      let errors = [],
        // to store success of creating file in directory
        created = [],
        // to store no of times attempted to create file in directory
        executionCount = 0;
      fileNames.forEach((currentFile) => {
        let filePath = path.join(directoryPath, currentFile);
        fs.writeFile(
          filePath,
          JSON.stringify(ContentOfJsonFile, null, 2),
          'utf8',
          (err) => {
            executionCount++;
            if (err) {
              errors.push(err);
            } else {
              console.log(`File with path ${filePath} successfully created`);
              created.push(filePath);
            }

            if (executionCount == fileNames.length) {
              // If errors array is not empty means that all files are not created successfully
              if (errors.length > 0) {
                reject(errors);
              } else {
                resolve(created);
              }
            }
          }
        );
      });
    }
  });
}

// Function to delete files store in Json directory.
function deleteJsonFiles(filePaths) {
  return new Promise((resolve, reject) => {
    // to store errors while deleting file from directory
    let errors = [],
      // to store no of times attempted to delete file from directory
      executionCount = 0;
    filePaths.forEach((currentPath) => {
      fs.unlink(currentPath, (err) => {
        executionCount++;
        if (err) {
          errors.push(err);
        } else {
          console.log(`File with path ${currentPath} successfully deleted`);
        }

        if (executionCount == filePaths.length) {
          // If errors array is not empty means that all files are not deleted successfully
          if (errors.length > 0) {
            reject(err);
          } else {
            resolve('All Json Files Successfully deleted from directory');
          }
        }
      });
    });
  });
}

module.exports = { createJSONDirectory, createJSONFiles, deleteJsonFiles };
