/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require('fs');
const path = require('path');

// Function to read files.
function readFileContent(fileName) {
  return new Promise((resolve, reject) => {
    if (fileName === undefined) {
      reject('File name entered is undefined');
    } else if (typeof fileName !== 'string') {
      reject(`${fileName} entered file name is not of type string`);
    } else {
      const lipsumPath = path.join(__dirname, fileName);
      fs.readFile(lipsumPath, 'utf8', (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    }
  });
}

// Function to append content to a new file created
function appendToNewFile(content, fileName) {
  return new Promise((resolve, reject) => {
    if (fileName === undefined) {
      reject('File name entered is undefined');
    } else if (typeof fileName !== 'string') {
      reject(`${fileName} entered file name is not of type string`);
    } else {
      const filePath = path.join(__dirname, fileName);
      fs.appendFile(filePath, content + '\n', 'utf8', (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(filePath);
        }
      });
    }
  });
}

// Function to delete all newly created files.
function deleteJsonFiles(filesPathArray) {
  return new Promise((resolve, reject) => {
    if (filesPathArray.length === 0) {
      reject('There are no files to be deleted');
    } else {
      // to store errors while deleting file
      let errors = [],
        // to store no of times attempted to delete file.
        executedCount = 0;

      filesPathArray.forEach((currentFile) => {
        fs.unlink(currentFile, (err) => {
          executedCount++;
          if (err) {
            errors.push(err);
          } else {
            console.log(`File with path ${currentFile} successfully deleted`);
          }

          if (executedCount == filesPathArray.length) {
            // if errors are there means all files are not deleted
            if (errors.length > 0) {
              reject(errors);
            } else {
              resolve(
                'All Json files which are created are successfully deleted'
              );
            }
          }
        });
      });
    }
  });
}

module.exports = {
  readFileContent,
  appendToNewFile,
  deleteJsonFiles,
};
