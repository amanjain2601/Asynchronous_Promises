const returnedFunctions = require('../problem1.js');

returnedFunctions
  .createJSONDirectory('Json_Files')
  .then((directoryPath) => {
    console.log(`Directory Successfully created with path ${directoryPath}`);
    return returnedFunctions.createJSONFiles(directoryPath, [
      '1.json',
      '2.json',
      '3.json',
    ]);
  })
  .then((FilePaths) => {
    console.log('All Json Files successfully created in directory');
    return returnedFunctions.deleteJsonFiles(FilePaths);
  })
  .then((message) => {
    console.log(message);
    console.log('Success');
  })
  .catch((err) => {
    console.error('Error is:', err);
  });
