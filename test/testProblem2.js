const returnedFunctions = require('../problem2.js');
// Read Content of lipsum.txt
returnedFunctions
  .readFileContent('lipsum.txt')
  .then((lipsumData) => {
    console.log('Data successfully readed from lipsum.txt');
    // append content of lipsum.txt to new file 1.txt
    return returnedFunctions.appendToNewFile(lipsumData.toUpperCase(), '1.txt');
  })
  .then((newFilePath) => {
    console.log('Data of lipsum.txt successfully written to 1.txt');
    // append path of 1.txt to filenames.txt
    return returnedFunctions.appendToNewFile(newFilePath, 'filenames.txt');
  })
  .then((newFilePath) => {
    console.log(
      `File with path ${newFilePath} successfully stored in filenames.txt`
    );
    // read file 1.txt
    return returnedFunctions.readFileContent('1.txt');
  })
  .then((newFileContent) => {
    // convert content to 1.txt to lowercase
    newFileContent = newFileContent.toLowerCase();
    console.log('Data successfully read from 1.txt and converted to lowercase');
    // split the content of 1.txt into sentences
    const contentSplittedToSentences = newFileContent.split('.').join('\n');
    // append the content splitted to sentences of 1.txt to 2.txt
    return returnedFunctions.appendToNewFile(
      contentSplittedToSentences,
      '2.txt'
    );
  })
  .then((newFilePath) => {
    console.log(
      'Data of 1.txt successfully splitted to sentences and written to 2.txt'
    );
    // append path of 2.txt to filenames.txt
    return returnedFunctions.appendToNewFile(newFilePath, 'filenames.txt');
  })
  .then((newFilePath) => {
    console.log(
      `File with path ${newFilePath} successfully stored in filenames.txt`
    );
    // read content of 1.txt
    return returnedFunctions.readFileContent('1.txt');
  })
  .then((newFileContent) => {
    // sort the content of 1.txt
    const sortedContent = newFileContent.split(' ').sort().join(' ');
    console.log('Data of 1.txt successfully readed and sorted');
    // append sorted content of 1.txt to 3.txt
    return returnedFunctions.appendToNewFile(sortedContent, '3.txt');
  })
  .then((newFilePath) => {
    console.log('Sorted data of 1.txt successfully appended to 3.txt');
    // read file 2.txt
    return returnedFunctions.readFileContent('2.txt');
  })
  .then((newFileContent) => {
    // sort content of 2.txt
    const sortedContent = newFileContent.split('\n').sort().join('\n');
    console.log('Data of 2.txt successfully readed and sorted');
    // append sorted content of 2.txt to 3.txt
    return returnedFunctions.appendToNewFile(sortedContent, '3.txt');
  })
  .then((newFilePath) => {
    console.log('Sorted data of 2.txt successfully appended to 3.txt');
    // append filepath of 3.txt to filenames.txt
    return returnedFunctions.appendToNewFile(newFilePath, 'filenames.txt');
  })
  .then((newFilePath) => {
    console.log(
      `File with path ${newFilePath} successfully stored in filenames.txt`
    );
    // read content of filenames.txt
    return returnedFunctions.readFileContent('filenames.txt');
  })
  .then((fileContent) => {
    // get file paths to delete newly created file
    filesPathArray = fileContent.split('\n');
    filesPathArray = filesPathArray.slice(0, filesPathArray.length - 1);
    console.log('File paths successfully readed from filenames.txt');
    // delete all files stored in array
    return returnedFunctions.deleteJsonFiles(filesPathArray);
  })
  .then((message) => {
    console.log(message);
    console.log('Success');
  })
  .catch((err) => {
    console.error('Error is:', err);
  });
